# TicTacToe
Simple Spring Boot Tic Tac Toe Game With Adjustable Board Size

#Requirement
1. Java 8
2. Maven 3.6.1

#How to Run
1. Open terminal
2. `cd to/project/path`
3. Run command `mvn spring-boot:run`
4. Open `localhost:8080/tictactoe` to access the game
5. Choose your board Size
6. Choose whether to play against `human` or `computer (randomized)`
7. Play!
8. To restart the game press `New Game`
9. To adjust the board size press `Start Over`