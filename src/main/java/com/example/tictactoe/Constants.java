package com.example.tictactoe;

public class Constants {
	public static final String GAME_STATE = "gameState";

	public static final String VIEW_GAME = "tictactoe";
	public static final String BOARD_SIZE = "board_size";
}
